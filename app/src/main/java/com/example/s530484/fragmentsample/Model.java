package com.example.s530484.fragmentsample;

/**
 * Created by S530484 on 2/27/2018.
 */

public class Model {
    private static Model myModel=null;


    public static Model getInstance()
    {

        if(myModel == null)
            myModel=new Model();
        return myModel;
    }

    private int x;

    public Model() {
        this.x = 0;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int addOne()
    {
        return x++;
    }
}
