package com.example.s530484.fragmentsample;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment2 extends Fragment {


    public Fragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_fragment2, container, false);
        View v=inflater.inflate(R.layout.fragment_fragment2, container, false);
        Button b=v.findViewById(R.id.press);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Model ref=Model.getInstance();
                ref.addOne();
                Log.d("Click","Added One "+Model.getInstance().getX());
            }
        });

        return v;
    }

}
